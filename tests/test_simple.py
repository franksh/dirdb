
from pathlib import Path
import time
import numpy as np
from dirdb import dirdb
import tempfile

np.set_printoptions(suppress=True, precision=4)

dirname = tempfile.mkdtemp()
name = 'test'

def test_store():
  print(f"using temporary directory: {dirname}")
  db = dirdb(dirname)

  # Database starts out empty.
  assert list(db) == []

  with db['1'] as entry:
    # entry doesn't exist yet.
    assert not entry

    # Some random data.
    data = np.random.rand(500)
    remember_me = data.min()

    entry.put_data(data, meta=dict(shape=data.shape, member=remember_me))

  assert len(list(db)) == 1
  assert '1' in db

  e = db['2']
  e.put_data([1,2])

  assert len(list(db)) == 2

  with db['1'] as entry:
    assert bool(entry)

    arr = entry.get_data()
    # Basic consistency.
    assert arr.min() == entry.meta.member

    entry.meta.test = "hello"

  # It should have been saved.
  assert db['1'].meta.test == "hello"

  assert not bool(db['2'].meta)


if __name__ == '__main__':
  test_dirdb()
