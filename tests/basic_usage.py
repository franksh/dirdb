import numpy as np
from dirdb import dirdb

# A database is simply a valid directory path. It will be created if
# it doesn't exist. Here we create the directory `./testdb`.
db = dirdb('testdb')

# It acts a lot like a dict() and returns "database entry" which is a
# proxy element that lets you inspect the entry's JSON metadata and
# can be used to load the associated data set.

assert "foo" not in db
entry = db["foo"]

# Note that entry also boolean-evaluates to False because it has no
# data evaluated with it.
assert not entry

# Once we put some data into it, the data will go into
# `./testdb/foo.pickle` and the `.meta` attribute (if not None) will
# be stored in `./testdb/foo.json`.
entry.put_data(np.random.rand(5,5), meta={'shape': (5,5)})

assert (entry.name in db) and entry

# These entry objects can be used in a `with:` statement to lock them
# (optional, but recommended).
with entry:
  # Deletes the meta data. (This action will be flushed to disk.)
  entry.meta = None

  # This is also flushed.
  entry.meta = {'test': [1,2]}

  # The meta dict attribute acts sort of like a javascript object.
  # Meta data is not flushed here.
  entry.meta.test.append(3)

  # But it will be flushed upon exit of the with: block --v

# Saves some data without .json metadata.
db['bar'].put_data([1] * 1000)

# Later, reloading the data:
with db['foo'] as e:
  # Inspect the metadata (loads the .json file):
  print("the updated meta:", e.meta)

  # Retrieve the data:
  print("data was:\n", e.get_data())

# Iterate over elements in the directory.
for entry in db:
  with entry:
    print(f"{entry.name} w/ meta data {str(entry.meta)}")
